﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class JournalEntry
    {

        public string CollectionName { get; set; }
        public string EventName { get; set; }
        public int Number { get; set; }


        public JournalEntry()
        {
            CollectionName = "";
            EventName = "";
            Number = 0;
        }

        public override string ToString()
        {
            return string.Format("[TeamsJournalEntry: CollectionName={0}, EventName={1}, Number={2}]", CollectionName, EventName, Number);
        }
    }
}
