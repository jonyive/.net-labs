﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    enum TimeFrame { Year, TwoYears, Long };

    class Person : System.IComparable, IComparer<Person>
    {
        protected string _name;
        protected string _surname;
        protected DateTime _dateOfBirth;

        public string Name
        {
            get => _name;
            set => _name = value;
        }

        public string Surname
        {
            get => _surname;
            set => _surname = value;
        }

        public DateTime DateOfBirth
        {
            get => _dateOfBirth;
            set => _dateOfBirth = value;
        }

        public Person() : this("Default", "Person", new DateTime(1998, 10, 10))
        {
        }

        public Person(string name, string surname, DateTime dateOfBirth)
        {
            _name = name;
            _surname = surname;
            _dateOfBirth = dateOfBirth;
        }

        public override string ToString()
        {
            return "Person : { name: " + Name + ", surname: " + Surname + ", birthDate: " + DateOfBirth + "}";
        }

        public virtual string ToShortString()
        {
            return Name + " " + Surname;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            Person other = obj as Person;
            if (other == null) return false;
            if (!string.Equals(_name, other._name)) return false;
            if (!string.Equals(_surname, other._surname)) return false;
            if (!_dateOfBirth.Equals(other._dateOfBirth)) return false;
            return true;
        }

        public static bool operator ==(Person left, Person right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Person left, Person right)
        {
            return !Equals(left, right);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (_name != null ? _name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (_surname != null ? _surname.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ _dateOfBirth.GetHashCode();
                return hashCode;
            }
        }

        public virtual object DeepCopy()
        {
            return MemberwiseClone();
        }

        public int CompareTo(object obj)
        {
                Person other = obj as Person;
                if (other != null)
                {
                    return String.Compare(_surname, other._surname);
                }
                else
                {
                    throw new System.ArgumentException("Something went wrong");
                }
        }

        public int Compare(Person x, Person y)
        {
            return DateTime.Compare(x.DateOfBirth, y.DateOfBirth);
        }
    }
}