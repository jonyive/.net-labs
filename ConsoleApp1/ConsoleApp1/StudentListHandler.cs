﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public delegate void StudentListHandler(object source, StudentListHandlerEventArgs args);

    public class StudentListHandlerEventArgs : System.EventArgs
    {
        public string Name { get; set; }
        public string Info { get; set; }
        public int Index { get; set; }

        public StudentListHandlerEventArgs()
        {
            Name = string.Empty;
            Info = string.Empty;
            Index = -1;
        }
        public StudentListHandlerEventArgs(string withName)
        {
            Name = withName;
            Info = string.Empty;
            Index = -1;

        }
        public StudentListHandlerEventArgs(string withName, string withInfo)
        {
            Name = withName;
            Info = withInfo;
            Index = -1;
        }
        public StudentListHandlerEventArgs(string withName, string withInfo, int withIndex)
        {
            Name = withName;
            Info = withInfo;
            Index = withIndex;
        }

        public override string ToString()
        {
            return "StudentListHandlerEventArgs : { name: " + Name + ", Info: " + Info + ", Info:" + Info + " }";
        }
    }
}
