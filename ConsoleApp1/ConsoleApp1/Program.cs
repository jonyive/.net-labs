﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student();

            Exam ex1 = new Exam("Dot Net", 5, new DateTime(2018, 11, 19));
            Exam ex2 = new Exam("Python", 5, new DateTime(2018, 11, 20));
            Exam ex3 = new Exam("Dot Net", 5, new DateTime(2018, 11, 21));

            student.AddExam(ex1);
            student.AddExam(ex2);
            student.AddExam(ex3);

            Student studCopy = (Student)student.DeepCopy();

            Console.WriteLine("Real object");
            Console.WriteLine(student.ToString());
            Console.WriteLine("Copy");
            Console.WriteLine(studCopy.ToString());

            Console.WriteLine("Write file name: ");
            string filename = Console.ReadLine() + ".dat";
            try
            {
                FileStream fileOpen = new FileStream(filename, FileMode.Open, FileAccess.Write, FileShare.Write);
                fileOpen.Close();
                Console.WriteLine(student.Load(filename));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                FileStream file = new FileStream(filename, FileMode.Create);
                file.Close();
            }

            Console.WriteLine("after save");
            Console.WriteLine(student.ToString());

            Console.WriteLine(student.AddFromConsole());
            Console.WriteLine(student.Save(filename));
            Console.WriteLine("add from console");
            Console.WriteLine(student.ToString());

            Student test = new Student();
            Console.WriteLine(Student.Load(filename, student));
            Console.WriteLine(student.AddFromConsole());
            Console.WriteLine("add from console and save");
            Console.WriteLine(Student.Save(filename, student));

            Console.WriteLine("after save");
            Console.WriteLine(student.ToString());
            

            Console.ReadKey();
        }
    }
}
