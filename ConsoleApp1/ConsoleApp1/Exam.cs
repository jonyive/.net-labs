﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Exam : IDateAndCopy
    {

        public string _examName { get; set; }
        public int _examMark { get; set; }
        public DateTime _examDate { get; set; }

        public Exam() : this("Matan", 2, new DateTime(2018, 10, 10))
        {
        }

        public Exam(string examName, int examMark, DateTime examDate)
        {
            _examName = examName;
            _examMark = examMark;
            _examDate = examDate;
        }

        public int Mark
        {
            get => _examMark;
            set => _examMark = value;
        }

        public DateTime Date { get; set; }

        public override string ToString()
        {
            return "Exam : { name: " + _examName + ", mark: " + _examMark + ", date: " + _examDate + "}";
        }

        public virtual object DeepCopy()
        {
            return MemberwiseClone();
        }
    }
}