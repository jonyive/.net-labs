﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class StudentCompare : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return (int)x.avgMark - (int)y.avgMark;
        }
    }
}
