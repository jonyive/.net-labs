﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Journal
    {
        private System.Collections.Generic.List<JournalEntry> _list;

        public Journal()
        {
            _list = new System.Collections.Generic.List<JournalEntry>();
        }

        public void Handler(object source, StudentListHandlerEventArgs args)
        {
            JournalEntry e = new JournalEntry();
            e.CollectionName = args.Info;
            e.EventName = args.Name;
            e.Number = args.Index;
            _list.Add(e);
        }

        public override string ToString()
        {
            string str = "";
            foreach (JournalEntry item in _list)
            {
                str += "\n" + item.EventName + " " + item.CollectionName + " " + item.Number;
            }
            return "Journal: { " + str + " }\n";
        }
    }
}
