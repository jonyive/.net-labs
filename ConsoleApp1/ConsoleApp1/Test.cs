﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Test
    {
        protected string _name;
        protected bool _passed;

        public string Name
        {
            get => _name;
            set => _name = value;
        }

        public bool Passed
        {
            get => _passed;
            set => _passed = value;
        }

        public Test(string name, bool passed)
        {
            Name = name;
            Passed = passed;
        }

        public Test() : this("Dot Net", true){}

        public virtual object DeepCopy()
        {
            return MemberwiseClone();
        }

        public override string ToString()
        {
            return "Test : { name: " + Name + ", passed: " + _passed + "}";
        }

    }
}
