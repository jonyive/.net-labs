﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class TestCollections
    {
        List<Person> _a;
        List<string> _b;
        Dictionary<Person, Student> _c;
        Dictionary<string, Student> _d;

        public TestCollections()
        {
            _a = new List<Person>(4);
            _b = new List<string>(4);
            _c = new Dictionary<Person, Student>(4);
            _d = new Dictionary<string, Student>(4);
        }

        public TestCollections(int count)
        {
            _a = new List<Person>(count);
            _b = new List<string>(count);
            _c = new Dictionary<Person, Student>(count);
            _d = new Dictionary<string, Student>(count);
        }

        public Person this[int index] { get { return _a[index]; } }

        public int GetTiming()
        {
            SetDefaults();
            Console.WriteLine("\n\n--------List-------\n");
            Person person = this[0];
            var taskBegin = Environment.TickCount;
            _a.Contains(person);
            var taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for first: {0}", taskEnd - taskBegin);

            person = this[1];
            taskBegin = Environment.TickCount;
            _a.Contains(person);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for second: {0}", taskEnd - taskBegin);

            person = this[2];
            taskBegin = Environment.TickCount;
            _a.Contains(person);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for third: {0}", taskEnd - taskBegin);

            person = this[3];
            taskBegin = Environment.TickCount;
            _a.Contains(person);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for fourth: {0}", taskEnd - taskBegin);


            Console.WriteLine("\n\n----Dictionary Keys----\n");
            Person[] keys = new Person[4];
            _c.Keys.CopyTo(keys, 0);

            taskBegin = Environment.TickCount;
            _c.ContainsKey(keys[0]);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for first: {0}", taskEnd - taskBegin);

            taskBegin = Environment.TickCount;
            _c.ContainsKey(keys[1]);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for second: {0}", taskEnd - taskBegin);

            taskBegin = Environment.TickCount;
            _c.ContainsKey(keys[2]);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for third: {0}", taskEnd - taskBegin);

            taskBegin = Environment.TickCount;
            _c.ContainsKey(keys[3]);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for fourth: {0}", taskEnd - taskBegin);


            Console.WriteLine("\n\n----Dictionary Values----\n");
            Student[] values = new Student[4];
            _c.Values.CopyTo(values, 0);

            taskBegin = Environment.TickCount;
            _c.ContainsValue(values[0]);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for first: {0}", taskEnd - taskBegin);

            taskBegin = Environment.TickCount;
            _c.ContainsValue(values[1]);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for second: {0}", taskEnd - taskBegin);

            taskBegin = Environment.TickCount;
            _c.ContainsValue(values[2]);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for third: {0}", taskEnd - taskBegin);

            taskBegin = Environment.TickCount;
            _c.ContainsValue(values[3]);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for fourth: {0}", taskEnd - taskBegin);

            return 0;
        }

        private void SetDefaults()
        {
            _a.Add(new Person("Ivan", "Ivanenko", new DateTime()));
            _a.Add(new Person("Ivan2", "Ivanenko2", new DateTime()));
            _a.Add(new Person("Ivan3", "Ivanenko3", new DateTime()));
            _a.Add(new Person());

            _c.Add(_a[0], new Student("John", "Paul", new DateTime(), Education.Bachelor, 402));
            _c.Add(_a[1], new Student("John2", "Pau2", new DateTime(), Education.Master, 202));
            _c.Add(_a[2], new Student("John3", "Pau3", new DateTime(), Education.SecondEducation, 302));
            _c.Add(new Person(), new Student());
        }
    }
}
