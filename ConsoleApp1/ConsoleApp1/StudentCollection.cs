﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class StudentCollection
    {
        System.Collections.Generic.List<Student> _list;

        public double MaxAvgMark { get { return (_list != null && _list.Count() > 0) ? _list.Min().avgMark : 0; } }
        public IEnumerable<Student> Masters { get { return _list.Where((Student student) => student.Education == Education.Master); } }

        public event StudentListHandler StudentCountChanged;
        public event StudentListHandler StudentReferenceChanged;

        public StudentCollection()
        {
        }

        public Student this[int index]
        {
            get
            {
                return _list[index];
            }

            set
            {
                _list[index] = value;
            }
        }


        public bool Remove(int index)
        {
            if (index <= 0 || index > _list.Count())
            {
                _list.RemoveAt(index);
            }
            else return false;

            StudentListHandlerEventArgs args = new StudentListHandlerEventArgs();
            args.Name = "Remove";
            args.Info = "Student added";
            args.Index = index;
            if (StudentCountChanged != null)
            {
                StudentCountChanged(this, args);
            }
            return true;
        }



        public void AddDefaults()
        {
            if (_list != null)
            {
                _list.Add(new Student());
            }
            else
            {
                _list = new System.Collections.Generic.List<Student>(4);
                _list.Add(new Student());
            }

            StudentListHandlerEventArgs args = new StudentListHandlerEventArgs();
            args.Name = "Insert";
            args.Info = "Student added";
            args.Index = _list.Count - 1;
            if (StudentCountChanged != null)
            {
                StudentCountChanged(this, args);
            }
        }

        public void AddStudents(params Student[] students)
        {
            if (_list != null)
            {
                _list.AddRange(students);
            }
            else
            {
                _list = new System.Collections.Generic.List<Student>(4);
                _list.AddRange(students);
            }

            StudentListHandlerEventArgs args = new StudentListHandlerEventArgs();
            args.Name = "Insert";
            args.Info = "Student added";
            args.Index = _list.Count - 1;
            if (StudentCountChanged != null)
            {
                StudentCountChanged(this, args);
            }
        }

        public override string ToString()
        {
            StringBuilder s1 = new StringBuilder();
            s1.Append("Students: {\n");
            foreach (Student student in _list)
            {
                s1.Append(student.ToString());
                s1.Append("\n}");
            }
            return s1.ToString();
        }

        public virtual string ToShortList()
        {
            string s = "";
            foreach (Student student in _list)
            {
                s += student.ToShortString() + "\t";
            }
            return string.Format("[StudentsCollection: {0}]", s);
        }

        public void Sort(int how)
        {
            switch (how)
            {
                case 1:
                    _list.Sort();
                    break;
                case 2:
                    _list.Sort((x, y) => x.CompareTo(y));
                    break;
                case 3:
                    StudentCompare c = new StudentCompare();
                    _list.Sort((x, y) => c.Compare(x, y));
                    break;
                default:
                    break;
            }
        }

        public List<Student> AvgGroupMark(int value)
        {
            var l = _list.GroupBy(team => team.avgMark == value)
                    .Select(g => new
                    {
                        Items = g.ToList()
                    }).ToList();
            return l[0].Items;
        }


    }
}
