﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    [Serializable]
    class Student : Person
    {
        private Education _education { get; set; }
        private int _group { get; set; }
        private ArrayList _testList = new ArrayList();
        private ArrayList _examList = new ArrayList();

        public Student(string name, string surname, DateTime dateofBirthday, Education education, int group) : base(
            name, surname, dateofBirthday)
        {
            Education = education;
            Group = group;
        }

        public Student() : this("Petro", "Poroshenko", new DateTime(1954, 9, 23), Education.Bachelor, 402)
        {
        }

        public Education Education
        {
            get => _education;
            set => _education = value;
        }

        public int Group
        {
            get => _group;
            set { if (value <= 100 || value >= 699) throw new InvalidOperationException("Group number must be >= 100 and <= 699. Thank You!");  _group = value; }
        }


        public Person Person
        {
            get => new Person(_name, _surname, _dateOfBirth);
            set
            {
                Name = value.Name;
                Surname = value.Surname;
                DateOfBirth = value.DateOfBirth;
            }
        }

        public ArrayList TestList
        {
            get => _testList ?? (_testList = new ArrayList());
            set => _testList = value;
        }

        public ArrayList ExamList
        {
            get => _examList ?? (_examList = new ArrayList());
            set => _examList = value;
        }

        public double avgMark
         {
             get
             {
                double sum = 0;
                if (_examList.Count == 0)
                    return sum;
                else
                    for (int j = 0; j < _examList.Count; j++)
                    {
                        sum += ((Exam)_examList[j]).Mark;
                    }
                 return sum / _examList.Count;
             }
         }


        public void AddExam(Exam exam)
        {
            ExamList.Add(exam);
        }

        public void AddTest(Test test)
        {
            TestList.Add(test);
        }

        public bool checkEducation (Education education)
        {
            return _education.Equals(education);
        }

        public override string ToString()
        {
            StringBuilder s1 = new StringBuilder();
            s1.Append("Student: {" + Person.ToString()
                + ", education: " + _education.ToString()
                + ", group: " + _group
                + ", exams: ");
            for (int i = 0; i < ExamList.Count; i++)
            {
                s1.Append(ExamList[i].ToString());
                s1.Append("\n");
            }
            s1.Append("tests: ");

            foreach (Test test in TestList)
            {
                s1.Append(test.ToString());
                s1.Append("\n");
            }
            return s1.ToString();
        }

        public override string ToShortString()
        {
            StringBuilder s1 = new StringBuilder();
            s1.Append("Student: {" + Person.ToString()
                + ", education: " + _education.ToString()
                + ", group: " + _group
                + ", avgMark: " + this.avgMark);
            return s1.ToString();
        }


        public override object DeepCopy()
        {
            Student tmpStudent = new Student()
            {
                Name = this.Name,
                Surname = this.Surname,
                DateOfBirth = this.DateOfBirth,
                Group = this.Group,
                TestList = this.TestList,
                ExamList = this.ExamList,
            };

            tmpStudent.TestList = new ArrayList();
            foreach (Test test in TestList)
            {
                tmpStudent.TestList.Add(test.DeepCopy());
            }
            tmpStudent.ExamList = new ArrayList();
            foreach (Exam exam in ExamList)
            {
                tmpStudent.ExamList.Add(exam.DeepCopy());
            }
            return tmpStudent;
        }

        public IEnumerable GetTestsEnumerator()
        {
            for (int i = 0; i < _testList.Count; i++)
            {
                        yield return ((Test)_testList[i]);
            }
        }

        public IEnumerable GetExamsByMarkEnumerator(int mark)
        {
            for (int j = 0; j < _examList.Count; j++)
            {
                if (((Exam)_examList[j]).Mark > mark)
                    yield return ((Exam)_examList[j]);
            }

        }

        public int Compare(Student x, Student y)
        {
            return String.Compare(x.Surname, y.Surname);
        }

        public bool Save(string filename)
        {
            Student Deser = new Student();
            BinaryFormatter serealizable = new BinaryFormatter();
            FileStream fileSerialize = File.Open(filename, FileMode.Open);
            try
            {
                serealizable.Serialize(fileSerialize, this);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                fileSerialize.Close();
                return false;
            }
            fileSerialize.Close();
            FileStream fileDeserialize = File.Open(filename, FileMode.Open);
            try
            {
                Deser = serealizable.Deserialize(fileDeserialize) as Student;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                fileDeserialize.Close();
                return false;
            }
            fileDeserialize.Close();
            return true;
        }

        public bool Load(string filename)
        {
            BinaryFormatter deserializable = new BinaryFormatter();
            FileStream fileDeserializable = File.OpenRead(filename);
            Student temp;
            try
            {
                temp = deserializable.Deserialize(fileDeserializable) as Student;
                Load(filename, temp);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                fileDeserializable.Close();
                return false;
            }
            fileDeserializable.Close();
            return true;
        }

        public static bool Save(string filename, Student teamp)
        {
            BinaryFormatter serializer = new BinaryFormatter();
            FileStream fileSerializer = File.Create(filename);
            try
            {
                serializer.Serialize(fileSerializer, teamp);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                fileSerializer.Close();
                return false;
            }
            fileSerializer.Close();
            FileStream fileDeserializer = File.OpenRead(filename);
            try
            {
                Student temp = serializer.Deserialize(fileDeserializer) as Student;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                fileDeserializer.Close();
                return false;
            }
            fileDeserializer.Close();
            return true;
        }


        public static bool Load(string filename, Student student)
        {
            BinaryFormatter serializer = new BinaryFormatter();
            FileStream fileDeserialize = File.OpenRead(filename);
            Student temp;
            try
            {
                temp = serializer.Deserialize(fileDeserialize) as Student;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                fileDeserialize.Close();
                return false;
            }
            fileDeserialize.Close();
            student = temp;
            return true;
        }

        public bool AddFromConsole()
        {
            string input = Console.ReadLine();
            try
            {
                string[] parsedInput = input.Split(',');
                string name = parsedInput[0];
                string first = parsedInput[1];
                string second = parsedInput[2];
                DateTime date = Convert.ToDateTime(parsedInput[3]);
                this._testList.Add(new Test());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;

        }
    }

}
